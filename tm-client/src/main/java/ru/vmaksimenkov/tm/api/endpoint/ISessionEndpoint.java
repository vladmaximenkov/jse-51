package ru.vmaksimenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    boolean closeSession(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @WebMethod
    SessionRecord openSession(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

}
