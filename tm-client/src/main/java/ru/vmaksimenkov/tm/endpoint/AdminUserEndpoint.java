package ru.vmaksimenkov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-04-23T22:17:35.917+03:00
 * Generated source version: 3.4.2
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "AdminUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminUserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.vmaksimenkov.ru/AdminUserEndpoint/unlockUserByLoginRequest", output = "http://endpoint.tm.vmaksimenkov.ru/AdminUserEndpoint/unlockUserByLoginResponse")
    @RequestWrapper(localName = "unlockUserByLogin", targetNamespace = "http://endpoint.tm.vmaksimenkov.ru/", className = "ru.vmaksimenkov.tm.endpoint.UnlockUserByLogin")
    @ResponseWrapper(localName = "unlockUserByLoginResponse", targetNamespace = "http://endpoint.tm.vmaksimenkov.ru/", className = "ru.vmaksimenkov.tm.endpoint.UnlockUserByLoginResponse")
    public void unlockUserByLogin(

        @WebParam(name = "session", targetNamespace = "")
        ru.vmaksimenkov.tm.endpoint.SessionRecord session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vmaksimenkov.ru/AdminUserEndpoint/lockUserByLoginRequest", output = "http://endpoint.tm.vmaksimenkov.ru/AdminUserEndpoint/lockUserByLoginResponse")
    @RequestWrapper(localName = "lockUserByLogin", targetNamespace = "http://endpoint.tm.vmaksimenkov.ru/", className = "ru.vmaksimenkov.tm.endpoint.LockUserByLogin")
    @ResponseWrapper(localName = "lockUserByLoginResponse", targetNamespace = "http://endpoint.tm.vmaksimenkov.ru/", className = "ru.vmaksimenkov.tm.endpoint.LockUserByLoginResponse")
    public void lockUserByLogin(

        @WebParam(name = "session", targetNamespace = "")
        ru.vmaksimenkov.tm.endpoint.SessionRecord session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vmaksimenkov.ru/AdminUserEndpoint/removeUserByLoginRequest", output = "http://endpoint.tm.vmaksimenkov.ru/AdminUserEndpoint/removeUserByLoginResponse")
    @RequestWrapper(localName = "removeUserByLogin", targetNamespace = "http://endpoint.tm.vmaksimenkov.ru/", className = "ru.vmaksimenkov.tm.endpoint.RemoveUserByLogin")
    @ResponseWrapper(localName = "removeUserByLoginResponse", targetNamespace = "http://endpoint.tm.vmaksimenkov.ru/", className = "ru.vmaksimenkov.tm.endpoint.RemoveUserByLoginResponse")
    public void removeUserByLogin(

        @WebParam(name = "session", targetNamespace = "")
        ru.vmaksimenkov.tm.endpoint.SessionRecord session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );
}
