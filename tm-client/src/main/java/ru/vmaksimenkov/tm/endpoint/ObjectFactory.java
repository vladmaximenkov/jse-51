
package ru.vmaksimenkov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.vmaksimenkov.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ExistsUserByEmail_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "existsUserByEmail");
    private final static QName _ExistsUserByEmailResponse_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "existsUserByEmailResponse");
    private final static QName _ExistsUserByLogin_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "existsUserByLogin");
    private final static QName _ExistsUserByLoginResponse_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "existsUserByLoginResponse");
    private final static QName _RegistryUser_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "registryUser");
    private final static QName _RegistryUserResponse_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "registryUserResponse");
    private final static QName _UpdateUser_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "updateUser");
    private final static QName _UpdateUserPassword_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "updateUserPassword");
    private final static QName _UpdateUserPasswordResponse_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "updateUserPasswordResponse");
    private final static QName _UpdateUserResponse_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "updateUserResponse");
    private final static QName _ViewUser_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "viewUser");
    private final static QName _ViewUserResponse_QNAME = new QName("http://endpoint.tm.vmaksimenkov.ru/", "viewUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.vmaksimenkov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExistsUserByEmail }
     * 
     */
    public ExistsUserByEmail createExistsUserByEmail() {
        return new ExistsUserByEmail();
    }

    /**
     * Create an instance of {@link ExistsUserByEmailResponse }
     * 
     */
    public ExistsUserByEmailResponse createExistsUserByEmailResponse() {
        return new ExistsUserByEmailResponse();
    }

    /**
     * Create an instance of {@link ExistsUserByLogin }
     * 
     */
    public ExistsUserByLogin createExistsUserByLogin() {
        return new ExistsUserByLogin();
    }

    /**
     * Create an instance of {@link ExistsUserByLoginResponse }
     * 
     */
    public ExistsUserByLoginResponse createExistsUserByLoginResponse() {
        return new ExistsUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RegistryUser }
     * 
     */
    public RegistryUser createRegistryUser() {
        return new RegistryUser();
    }

    /**
     * Create an instance of {@link RegistryUserResponse }
     * 
     */
    public RegistryUserResponse createRegistryUserResponse() {
        return new RegistryUserResponse();
    }

    /**
     * Create an instance of {@link UpdateUser }
     * 
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link UpdateUserPassword }
     * 
     */
    public UpdateUserPassword createUpdateUserPassword() {
        return new UpdateUserPassword();
    }

    /**
     * Create an instance of {@link UpdateUserPasswordResponse }
     * 
     */
    public UpdateUserPasswordResponse createUpdateUserPasswordResponse() {
        return new UpdateUserPasswordResponse();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     * 
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link ViewUser }
     * 
     */
    public ViewUser createViewUser() {
        return new ViewUser();
    }

    /**
     * Create an instance of {@link ViewUserResponse }
     * 
     */
    public ViewUserResponse createViewUserResponse() {
        return new ViewUserResponse();
    }

    /**
     * Create an instance of {@link SessionRecord }
     * 
     */
    public SessionRecord createSessionRecord() {
        return new SessionRecord();
    }

    /**
     * Create an instance of {@link AbstractEntityRecord }
     * 
     */
    public AbstractEntityRecord createAbstractEntityRecord() {
        return new AbstractEntityRecord();
    }

    /**
     * Create an instance of {@link UserRecord }
     * 
     */
    public UserRecord createUserRecord() {
        return new UserRecord();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByEmail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ExistsUserByEmail }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "existsUserByEmail")
    public JAXBElement<ExistsUserByEmail> createExistsUserByEmail(ExistsUserByEmail value) {
        return new JAXBElement<ExistsUserByEmail>(_ExistsUserByEmail_QNAME, ExistsUserByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByEmailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ExistsUserByEmailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "existsUserByEmailResponse")
    public JAXBElement<ExistsUserByEmailResponse> createExistsUserByEmailResponse(ExistsUserByEmailResponse value) {
        return new JAXBElement<ExistsUserByEmailResponse>(_ExistsUserByEmailResponse_QNAME, ExistsUserByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ExistsUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "existsUserByLogin")
    public JAXBElement<ExistsUserByLogin> createExistsUserByLogin(ExistsUserByLogin value) {
        return new JAXBElement<ExistsUserByLogin>(_ExistsUserByLogin_QNAME, ExistsUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ExistsUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "existsUserByLoginResponse")
    public JAXBElement<ExistsUserByLoginResponse> createExistsUserByLoginResponse(ExistsUserByLoginResponse value) {
        return new JAXBElement<ExistsUserByLoginResponse>(_ExistsUserByLoginResponse_QNAME, ExistsUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RegistryUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "registryUser")
    public JAXBElement<RegistryUser> createRegistryUser(RegistryUser value) {
        return new JAXBElement<RegistryUser>(_RegistryUser_QNAME, RegistryUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RegistryUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "registryUserResponse")
    public JAXBElement<RegistryUserResponse> createRegistryUserResponse(RegistryUserResponse value) {
        return new JAXBElement<RegistryUserResponse>(_RegistryUserResponse_QNAME, RegistryUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "updateUser")
    public JAXBElement<UpdateUser> createUpdateUser(UpdateUser value) {
        return new JAXBElement<UpdateUser>(_UpdateUser_QNAME, UpdateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "updateUserPassword")
    public JAXBElement<UpdateUserPassword> createUpdateUserPassword(UpdateUserPassword value) {
        return new JAXBElement<UpdateUserPassword>(_UpdateUserPassword_QNAME, UpdateUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "updateUserPasswordResponse")
    public JAXBElement<UpdateUserPasswordResponse> createUpdateUserPasswordResponse(UpdateUserPasswordResponse value) {
        return new JAXBElement<UpdateUserPasswordResponse>(_UpdateUserPasswordResponse_QNAME, UpdateUserPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "updateUserResponse")
    public JAXBElement<UpdateUserResponse> createUpdateUserResponse(UpdateUserResponse value) {
        return new JAXBElement<UpdateUserResponse>(_UpdateUserResponse_QNAME, UpdateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ViewUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "viewUser")
    public JAXBElement<ViewUser> createViewUser(ViewUser value) {
        return new JAXBElement<ViewUser>(_ViewUser_QNAME, ViewUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ViewUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.vmaksimenkov.ru/", name = "viewUserResponse")
    public JAXBElement<ViewUserResponse> createViewUserResponse(ViewUserResponse value) {
        return new JAXBElement<ViewUserResponse>(_ViewUserResponse_QNAME, ViewUserResponse.class, null, value);
    }

}
