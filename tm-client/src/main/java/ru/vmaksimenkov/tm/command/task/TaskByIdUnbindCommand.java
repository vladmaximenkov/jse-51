package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIdUnbindCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Unbind task by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-unbind-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK BY ID]");
        if (endpointLocator.getTaskEndpoint().countTask(endpointLocator.getSession()) < 1)
            throw new TaskNotFoundException();
        System.out.println("ENTER TASK ID:");
        endpointLocator.getTaskEndpoint().unbindTaskById(endpointLocator.getSession(), TerminalUtil.nextLine());
    }

}
