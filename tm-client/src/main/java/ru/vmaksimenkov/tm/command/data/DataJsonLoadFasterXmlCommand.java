package ru.vmaksimenkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.command.AbstractCommand;

public final class DataJsonLoadFasterXmlCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @Nullable
    @Override
    public String commandDescription() {
        return "Load json data from file";
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-json-load";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        endpointLocator.getAdminEndpoint().loadJson(endpointLocator.getSession());
    }

}
