package ru.vmaksimenkov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.jms.LogMessage;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;

public class LogMessageListener implements MessageListener {

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
        if (!(entity instanceof LogMessage)) return;
        @NotNull final LogMessage logMessage = (LogMessage) entity;
        @NotNull final File file = new File(logMessage.getType() + ".log");
        file.createNewFile();
        @NotNull final FileOutputStream oFile = new FileOutputStream(file, true);
        oFile.write((logMessage.getEvent() + " : " + logMessage.getDate() + "\n" + logMessage.getJson() + "\n").getBytes());
        oFile.close();
    }

}
