package ru.vmaksimenkov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.bootstrap.Bootstrap;

public class Application {

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
