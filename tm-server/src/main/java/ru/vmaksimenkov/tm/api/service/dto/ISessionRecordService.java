package ru.vmaksimenkov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.enumerated.Role;

import java.util.List;

public interface ISessionRecordService extends IAbstractBusinessRecordService<SessionRecord> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    void close(@NotNull SessionRecord session);

    void closeAll(@NotNull SessionRecord session);

    @Nullable List<SessionRecord> getListSession(@NotNull SessionRecord session);

    @Nullable UserRecord getUser(@NotNull SessionRecord session);

    @NotNull String getUserId(@NotNull SessionRecord session);

    boolean isValid(@NotNull SessionRecord session);

    @Nullable SessionRecord open(@Nullable String login, @Nullable String password);

    @Nullable SessionRecord sign(@Nullable SessionRecord session);

    void validate(@NotNull SessionRecord session, @Nullable Role role);

    void validate(@Nullable SessionRecord session);

}
