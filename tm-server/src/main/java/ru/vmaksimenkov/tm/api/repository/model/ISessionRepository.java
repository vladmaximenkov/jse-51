package ru.vmaksimenkov.tm.api.repository.model;

import ru.vmaksimenkov.tm.model.Session;

public interface ISessionRepository extends IAbstractBusinessRepository<Session> {

}