package ru.vmaksimenkov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.ITaskRecordRepository;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRecordRepository extends AbstractBusinessRecordRepository<TaskRecord> implements ITaskRecordRepository {

    public TaskRecordRepository(@NotNull final EntityManager em) {
        super(em, TaskRecord.class);
    }

    @Override
    public void bindTaskPyProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final TaskRecord task = findById(userId, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
        update(task);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE TaskRecord where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE TaskRecord").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE userId = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return em.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE userId = :userId AND projectId = :projectId", Long.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<TaskRecord> findAll(@Nullable final String userId) {
        return em.createQuery("FROM TaskRecord WHERE userId = :userId", TaskRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskRecord> findAll() {
        return em.createQuery("FROM TaskRecord", TaskRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public List<TaskRecord> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return em.createQuery("FROM TaskRecord WHERE userId = :userId AND projectId = :projectId", TaskRecord.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskRecord findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM TaskRecord WHERE userId = :userId AND id = :id", TaskRecord.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public TaskRecord findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(em.createQuery("FROM TaskRecord WHERE userId = :userId AND name = :name", TaskRecord.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Override
    public void removeAllBinded(@Nullable final String userId) {
        em.createQuery("DELETE TaskRecord WHERE userId = :userId AND projectId IS NOT NULL").executeUpdate();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        em.createQuery("DELETE TaskRecord WHERE userId = :userId AND projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        em.createQuery("DELETE TaskRecord WHERE userId = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE userId = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM TaskRecord", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String id) {
        @Nullable final TaskRecord task = findById(userId, id);
        if (task == null) return;
        task.setProjectId(null);
        update(task);
    }

}
