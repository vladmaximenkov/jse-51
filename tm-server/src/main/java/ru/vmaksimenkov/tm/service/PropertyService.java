package ru.vmaksimenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

import static ru.vmaksimenkov.tm.constant.PropertyConst.*;

public class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getString(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public int getBackupInterval() {
        @Nullable final String environmentProperty = System.getenv(BACKUP_INTERVAL_KEY);
        if (environmentProperty != null) return Math.max(Integer.parseInt(environmentProperty), BACKUP_INTERVAL_MIN);
        @Nullable final String systemProperty = System.getProperty(BACKUP_INTERVAL_KEY);
        if (systemProperty != null) return Math.max(Integer.parseInt(systemProperty), BACKUP_INTERVAL_MIN);
        final int interval = Integer.parseInt(properties.getProperty(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT));
        return Math.max(interval, BACKUP_INTERVAL_MIN);
    }

    @NotNull
    @Override
    public String getCacheProvider() {
        return getString(HIBERNATE_CACHEPROVIDER_CONFIG_KEY, HIBERNATE_CACHEPROVIDER_CONFIG_DEFAULT);
    }

    @NotNull
    @Override
    public String getFactoryClass() {
        return getString(HIBERNATE_FACTORY_CLASS_KEY, HIBERNATE_FACTORY_CLASS_DEFAULT);
    }

    @NotNull
    @Override
    public String getFormatSQL() {
        return getString(HIBERNATE_FORMATSQL_KEY, HIBERNATE_FORMATSQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcDialect() {
        return getString(JDBC_DIALECT_KEY, JDBC_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return getString(JDBC_DRIVER_KEY, JDBC_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcHBM2DDL() {
        return getString(JDBC_HBM2DDL_KEY, JDBC_HBM2DDL_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return getString(JDBC_PASSWORD_KEY, JDBC_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcShowSql() {
        return getString(JDBC_SHOW_SQL_KEY, JDBC_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return getString(JDBC_URL_KEY, JDBC_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcUsername() {
        return getString(JDBC_USERNAME_KEY, JDBC_USERNAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getLiteMember() {
        return getString(HIBERNATE_LITEMEMBER_KEY, HIBERNATE_LITEMEMBER_DEFAULT);
    }

    @NotNull
    @Override
    public String getMinimalPuts() {
        return getString(HIBERNATE_MINPUTS_KEY, HIBERNATE_MINPUTS_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String environmentProperty = System.getenv(PASSWORD_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        return Integer.parseInt(
                properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT)
        );
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getString(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getQueryCache() {
        return getString(HIBERNATE_QCACHE_KEY, HIBERNATE_QCACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getRegionPrefix() {
        return getString(HIBERNATE_REGPREFIX_KEY, HIBERNATE_REGPREFIX_DEFAULT);
    }

    @Override
    public int getScannerInterval() {
        @Nullable final String environmentProperty = System.getenv(SCANNER_INTERVAL_KEY);
        if (environmentProperty != null) return Math.max(Integer.parseInt(environmentProperty), SCANNER_INTERVAL_MIN);
        @Nullable final String systemProperty = System.getProperty(SCANNER_INTERVAL_KEY);
        if (systemProperty != null) return Math.max(Integer.parseInt(systemProperty), SCANNER_INTERVAL_MIN);
        final int interval = Integer.parseInt(properties.getProperty(SCANNER_INTERVAL_KEY, SCANNER_INTERVAL_DEFAULT));
        return Math.max(interval, SCANNER_INTERVAL_MIN);
    }

    @NotNull
    @Override
    public String getSecondLevelCash() {
        return getString(HIBERNATE_SLCACHE_KEY, HIBERNATE_SLCACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getString(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public int getServerPort() {
        @Nullable final String environmentProperty = System.getenv(SERVER_PORT_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        @Nullable final String systemProperty = System.getProperty(SERVER_PORT_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        return Integer.parseInt(properties.getProperty(SERVER_PORT_KEY, SERVER_PORT_DEFAULT));
    }

    @Override
    public int getSessionCycle() {
        @Nullable final String environmentProperty = System.getenv(SESSION_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        @Nullable final String systemProperty = System.getProperty(SESSION_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        return Integer.parseInt(
                properties.getProperty(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT)
        );
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return getString(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @NotNull
    private String getString(@NotNull final String key, @NotNull final String defaultValue) {
        @Nullable final String environmentProperty = System.getenv(key);
        if (environmentProperty != null) return environmentProperty;
        @Nullable final String systemProperty = System.getProperty(key);
        if (systemProperty != null) return systemProperty;
        return properties.getProperty(key, defaultValue);
    }

}
