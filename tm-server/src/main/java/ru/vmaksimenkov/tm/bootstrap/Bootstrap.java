package ru.vmaksimenkov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.endpoint.*;
import ru.vmaksimenkov.tm.api.service.*;
import ru.vmaksimenkov.tm.api.service.dto.*;
import ru.vmaksimenkov.tm.api.service.model.IProjectTaskService;
import ru.vmaksimenkov.tm.api.service.model.IUserService;
import ru.vmaksimenkov.tm.component.Backup;
import ru.vmaksimenkov.tm.endpoint.*;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.service.AuthService;
import ru.vmaksimenkov.tm.service.ConnectionService;
import ru.vmaksimenkov.tm.service.LoggerService;
import ru.vmaksimenkov.tm.service.PropertyService;
import ru.vmaksimenkov.tm.service.dto.*;
import ru.vmaksimenkov.tm.service.model.ProjectTaskService;
import ru.vmaksimenkov.tm.service.model.UserService;
import ru.vmaksimenkov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final static String PID_FILENAME = "task-manager.pid";
    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final IProjectRecordService projectService = new ProjectRecordService(connectionService);
    @NotNull
    private final IProjectTaskRecordService projectTaskService = new ProjectTaskRecordService(connectionService);
    @NotNull
    private final IProjectTaskService projectTaskServiceGraph = new ProjectTaskService(connectionService);
    @NotNull
    private final Backup backup = new Backup(this, propertyService);
    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this, backup);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull
    private final ISessionRecordService sessionService = new SessionRecordService(connectionService, propertyService);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final ITaskRecordService taskService = new TaskRecordService(connectionService);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final IUserRecordService userService = new UserRecordService(connectionService, propertyService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);
    @NotNull
    private final IUserService userServiceGraph = new UserService(connectionService, propertyService);

    private void initData() {
        @NotNull final String idAdmin = userService.create("admin", "admin", Role.ADMIN).getId();
        @NotNull final String idTest = userService.create("test", "test", "test@test.ru").getId();

        projectService.add(idTest, "proj1", "desc1").setStatus(Status.COMPLETE);
        projectService.add(idTest, "proj2", "desc2").setStatus(Status.IN_PROGRESS);
        projectService.add(idTest, "proj3", "desc3").setStatus(Status.IN_PROGRESS);
        projectService.add(idAdmin, "proj4", "desc4").setStatus(Status.NOT_STARTED);
        projectService.add(idAdmin, "proj5", "desc5").setStatus(Status.COMPLETE);
        projectService.add(idAdmin, "proj6", "desc6").setStatus(Status.NOT_STARTED);

        taskService.add(idTest, "task1", "task_desc1").setStatus(Status.COMPLETE);
        taskService.add(idTest, "task2", "task_desc2").setStatus(Status.NOT_STARTED);
        taskService.add(idTest, "task3", "task_desc3").setStatus(Status.IN_PROGRESS);
        taskService.add(idAdmin, "task4", "task_desc4").setStatus(Status.NOT_STARTED);
        taskService.add(idAdmin, "task5", "task_desc5").setStatus(Status.IN_PROGRESS);
        taskService.add(idAdmin, "task6", "task_desc6").setStatus(Status.NOT_STARTED);
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(adminEndpoint);
        registry(adminUserEndpoint);
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILENAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILENAME);
        file.deleteOnExit();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        final int port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void run(@Nullable final String... args) {
        initPID();
        initData();
        initEndpoint();
        backup.init();
    }

}
